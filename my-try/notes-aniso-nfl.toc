\contentsline {title}{Non-Fermi Liquids From Concentric Fermi Surfaces}{1}{section*.2}
\contentsline {abstract}{Abstract}{1}{section*.1}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{2}{section*.3}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Introduction }{2}{section*.4}
\contentsline {section}{\numberline {II}Model in the low-doping regime}{3}{section*.5}
\contentsline {subsection}{\numberline {A}Superconducting Instability}{6}{section*.7}
\contentsline {subsection}{\numberline {B}Particle-Hole Susceptibility To SDW Order}{8}{section*.8}
\contentsline {section}{\numberline {III}Model in the high-doping regime}{9}{section*.9}
\contentsline {section}{\numberline {IV}Conclusion}{11}{section*.10}
\contentsline {section}{\numberline {}Acknowledgments}{11}{section*.11}
\appendix 
\contentsline {section}{\numberline {A}Computation Of The Self-Energy Diagrams At One-loop}{12}{section*.12}
\contentsline {subsection}{\numberline {1}One-loop Boson Self-Energy}{12}{section*.14}
\contentsline {subsection}{\numberline {2}One-loop Fermion Self-Energy}{13}{section*.15}
\contentsline {subsection}{\numberline {3}Fermion self-energy}{13}{section*.16}
\contentsline {subsection}{\numberline {4}Vertex renormalization}{13}{section*.17}
\contentsline {section}{\numberline {B}Some useful integrals}{14}{section*.18}
\contentsline {section}{\numberline {}References}{14}{section*.19}
