%merlin.mbs apsrev4-1.bst 2010-07-25 4.21a (PWD, AO, DPC) hacked
%Control: key (0)
%Control: author (8) initials jnrlst
%Control: editor formatted (1) identically to author
%Control: production of article title (-1) disabled
%Control: page (0) single
%Control: year (1) truncated
%Control: production of eprint (0) enabled
\begin{thebibliography}{51}%
\makeatletter
\providecommand \@ifxundefined [1]{%
 \@ifx{#1\undefined}
}%
\providecommand \@ifnum [1]{%
 \ifnum #1\expandafter \@firstoftwo
 \else \expandafter \@secondoftwo
 \fi
}%
\providecommand \@ifx [1]{%
 \ifx #1\expandafter \@firstoftwo
 \else \expandafter \@secondoftwo
 \fi
}%
\providecommand \natexlab [1]{#1}%
\providecommand \enquote  [1]{``#1''}%
\providecommand \bibnamefont  [1]{#1}%
\providecommand \bibfnamefont [1]{#1}%
\providecommand \citenamefont [1]{#1}%
\providecommand \href@noop [0]{\@secondoftwo}%
\providecommand \href [0]{\begingroup \@sanitize@url \@href}%
\providecommand \@href[1]{\@@startlink{#1}\@@href}%
\providecommand \@@href[1]{\endgroup#1\@@endlink}%
\providecommand \@sanitize@url [0]{\catcode `\\12\catcode `\$12\catcode
  `\&12\catcode `\#12\catcode `\^12\catcode `\_12\catcode `\%12\relax}%
\providecommand \@@startlink[1]{}%
\providecommand \@@endlink[0]{}%
\providecommand \url  [0]{\begingroup\@sanitize@url \@url }%
\providecommand \@url [1]{\endgroup\@href {#1}{\urlprefix }}%
\providecommand \urlprefix  [0]{URL }%
\providecommand \Eprint [0]{\href }%
\providecommand \doibase [0]{http://dx.doi.org/}%
\providecommand \selectlanguage [0]{\@gobble}%
\providecommand \bibinfo  [0]{\@secondoftwo}%
\providecommand \bibfield  [0]{\@secondoftwo}%
\providecommand \translation [1]{[#1]}%
\providecommand \BibitemOpen [0]{}%
\providecommand \bibitemStop [0]{}%
\providecommand \bibitemNoStop [0]{.\EOS\space}%
\providecommand \EOS [0]{\spacefactor3000\relax}%
\providecommand \BibitemShut  [1]{\csname bibitem#1\endcsname}%
\let\auto@bib@innerbib\@empty
%</preamble>
\bibitem [{\citenamefont {Holstein}\ \emph {et~al.}(1973)\citenamefont
  {Holstein}, \citenamefont {Norton},\ and\ \citenamefont {Pincus}}]{holstein}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {T.}~\bibnamefont
  {Holstein}}, \bibinfo {author} {\bibfnamefont {R.~E.}\ \bibnamefont
  {Norton}}, \ and\ \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont
  {Pincus}},\ }\href {\doibase 10.1103/PhysRevB.8.2649} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {8}},\
  \bibinfo {pages} {2649} (\bibinfo {year} {1973})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Reizer}(1989)}]{reizer}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.~Y.}\ \bibnamefont
  {Reizer}},\ }\href {\doibase 10.1103/PhysRevB.40.11571} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {40}},\
  \bibinfo {pages} {11571} (\bibinfo {year} {1989})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Lee}\ and\ \citenamefont {Nagaosa}(1992)}]{leenag}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {P.~A.}\ \bibnamefont
  {Lee}}\ and\ \bibinfo {author} {\bibfnamefont {N.}~\bibnamefont {Nagaosa}},\
  }\href {\doibase 10.1103/PhysRevB.46.5621} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {46}},\ \bibinfo
  {pages} {5621} (\bibinfo {year} {1992})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Halperin}\ \emph {et~al.}(1993)\citenamefont
  {Halperin}, \citenamefont {Lee},\ and\ \citenamefont {Read}}]{HALPERIN}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {B.~I.}\ \bibnamefont
  {Halperin}}, \bibinfo {author} {\bibfnamefont {P.~A.}\ \bibnamefont {Lee}}, \
  and\ \bibinfo {author} {\bibfnamefont {N.}~\bibnamefont {Read}},\ }\href
  {\doibase 10.1103/PhysRevB.47.7312} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {47}},\ \bibinfo
  {pages} {7312} (\bibinfo {year} {1993})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Polchinski}}(1994)}]{polchinski}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {J.}~\bibnamefont
  {{Polchinski}}},\ }\href {\doibase 10.1016/0550-3213(94)90449-9} {\bibfield
  {journal} {\bibinfo  {journal} {Nuclear Physics B}\ }\textbf {\bibinfo
  {volume} {422}},\ \bibinfo {pages} {617} (\bibinfo {year} {1994})},\ \Eprint
  {http://arxiv.org/abs/cond-mat/9303037} {cond-mat/9303037} \BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Altshuler}\ \emph {et~al.}(1994)\citenamefont
  {Altshuler}, \citenamefont {Ioffe},\ and\ \citenamefont
  {Millis}}]{ALTSHULER}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {B.~L.}\ \bibnamefont
  {Altshuler}}, \bibinfo {author} {\bibfnamefont {L.~B.}\ \bibnamefont
  {Ioffe}}, \ and\ \bibinfo {author} {\bibfnamefont {A.~J.}\ \bibnamefont
  {Millis}},\ }\href {\doibase 10.1103/PhysRevB.50.14048} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {50}},\
  \bibinfo {pages} {14048} (\bibinfo {year} {1994})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Kim}}\ \emph {et~al.}(2008)\citenamefont {{Kim}},
  \citenamefont {{Lawler}}, \citenamefont {{Oreto}}, \citenamefont {{Sachdev}},
  \citenamefont {{Fradkin}},\ and\ \citenamefont {{Kivelson}}}]{YBKim}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.-A.}\ \bibnamefont
  {{Kim}}}, \bibinfo {author} {\bibfnamefont {M.~J.}\ \bibnamefont {{Lawler}}},
  \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {{Oreto}}}, \bibinfo
  {author} {\bibfnamefont {S.}~\bibnamefont {{Sachdev}}}, \bibinfo {author}
  {\bibfnamefont {E.}~\bibnamefont {{Fradkin}}}, \ and\ \bibinfo {author}
  {\bibfnamefont {S.~A.}\ \bibnamefont {{Kivelson}}},\ }\href {\doibase
  10.1103/PhysRevB.77.184514} {\bibfield  {journal} {\bibinfo  {journal}
  {\prb}\ }\textbf {\bibinfo {volume} {77}},\ \bibinfo {eid} {184514} (\bibinfo
  {year} {2008})},\ \Eprint {http://arxiv.org/abs/0705.4099} {arXiv:0705.4099
  [cond-mat.supr-con]} \BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Nayak}}\ and\ \citenamefont
  {{Wilczek}}(1994)}]{nayak}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {C.}~\bibnamefont
  {{Nayak}}}\ and\ \bibinfo {author} {\bibfnamefont {F.}~\bibnamefont
  {{Wilczek}}},\ }\href {\doibase 10.1016/0550-3213(94)90158-9} {\bibfield
  {journal} {\bibinfo  {journal} {Nuclear Physics B}\ }\textbf {\bibinfo
  {volume} {430}},\ \bibinfo {pages} {534} (\bibinfo {year} {1994})},\ \Eprint
  {http://arxiv.org/abs/cond-mat/9408016} {cond-mat/9408016} \BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {{Lawler}}\ \emph {et~al.}(2006)\citenamefont
  {{Lawler}}, \citenamefont {{Barci}}, \citenamefont {{Fern{\'a}ndez}},
  \citenamefont {{Fradkin}},\ and\ \citenamefont {{Oxman}}}]{lawler1}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.~J.}\ \bibnamefont
  {{Lawler}}}, \bibinfo {author} {\bibfnamefont {D.~G.}\ \bibnamefont
  {{Barci}}}, \bibinfo {author} {\bibfnamefont {V.}~\bibnamefont
  {{Fern{\'a}ndez}}}, \bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {{Fradkin}}}, \ and\ \bibinfo {author} {\bibfnamefont {L.}~\bibnamefont
  {{Oxman}}},\ }\href {\doibase 10.1103/PhysRevB.73.085101} {\bibfield
  {journal} {\bibinfo  {journal} {\prb}\ }\textbf {\bibinfo {volume} {73}},\
  \bibinfo {eid} {085101} (\bibinfo {year} {2006})},\ \Eprint
  {http://arxiv.org/abs/cond-mat/0508747} {cond-mat/0508747} \BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Lee}(2009)}]{lee09}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont
  {Lee}},\ }\href {\doibase 10.1103/PhysRevB.80.165102} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {80}},\
  \bibinfo {pages} {165102} (\bibinfo {year} {2009})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Metlitski}\ and\ \citenamefont
  {Sachdev}(2010{\natexlab{a}})}]{metlsach1}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.~A.}\ \bibnamefont
  {Metlitski}}\ and\ \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Sachdev}},\ }\href {\doibase 10.1103/PhysRevB.82.075127} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {82}},\ \bibinfo {pages} {075127} (\bibinfo {year}
  {2010}{\natexlab{a}})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Metlitski}\ and\ \citenamefont
  {Sachdev}(2010{\natexlab{b}})}]{metlsach}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.~A.}\ \bibnamefont
  {Metlitski}}\ and\ \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Sachdev}},\ }\href {\doibase 10.1103/PhysRevB.82.075128} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {82}},\ \bibinfo {pages} {075128} (\bibinfo {year}
  {2010}{\natexlab{b}})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Abanov}}\ and\ \citenamefont
  {{Chubukov}}(2004)}]{chubukov1}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {{Abanov}}}\ and\ \bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {{Chubukov}}},\ }\href {\doibase 10.1103/PhysRevLett.93.255702} {\bibfield
  {journal} {\bibinfo  {journal} {Physical Review Letters}\ }\textbf {\bibinfo
  {volume} {93}},\ \bibinfo {eid} {255702} (\bibinfo {year}
  {2004})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Abanov}}\ and\ \citenamefont
  {{Chubukov}}(2000)}]{Chubukov}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {{Abanov}}}\ and\ \bibinfo {author} {\bibfnamefont {A.~V.}\ \bibnamefont
  {{Chubukov}}},\ }\href {\doibase 10.1103/PhysRevLett.84.5608} {\bibfield
  {journal} {\bibinfo  {journal} {Physical Review Letters}\ }\textbf {\bibinfo
  {volume} {84}},\ \bibinfo {pages} {5608} (\bibinfo {year} {2000})},\ \Eprint
  {http://arxiv.org/abs/cond-mat/0002122} {cond-mat/0002122} \BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Mross}\ \emph {et~al.}(2010)\citenamefont {Mross},
  \citenamefont {McGreevy}, \citenamefont {Liu},\ and\ \citenamefont
  {Senthil}}]{mross}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {D.~F.}\ \bibnamefont
  {Mross}}, \bibinfo {author} {\bibfnamefont {J.}~\bibnamefont {McGreevy}},
  \bibinfo {author} {\bibfnamefont {H.}~\bibnamefont {Liu}}, \ and\ \bibinfo
  {author} {\bibfnamefont {T.}~\bibnamefont {Senthil}},\ }\href {\doibase
  10.1103/PhysRevB.82.045121} {\bibfield  {journal} {\bibinfo  {journal} {Phys.
  Rev. B}\ }\textbf {\bibinfo {volume} {82}},\ \bibinfo {pages} {045121}
  (\bibinfo {year} {2010})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Jiang}}\ \emph {et~al.}(2013)\citenamefont
  {{Jiang}}, \citenamefont {{Block}}, \citenamefont {{Mishmash}}, \citenamefont
  {{Garrison}}, \citenamefont {{Sheng}}, \citenamefont {{Motrunich}},\ and\
  \citenamefont {{Fisher}}}]{Jiang}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {H.-C.}\ \bibnamefont
  {{Jiang}}}, \bibinfo {author} {\bibfnamefont {M.~S.}\ \bibnamefont
  {{Block}}}, \bibinfo {author} {\bibfnamefont {R.~V.}\ \bibnamefont
  {{Mishmash}}}, \bibinfo {author} {\bibfnamefont {J.~R.}\ \bibnamefont
  {{Garrison}}}, \bibinfo {author} {\bibfnamefont {D.~N.}\ \bibnamefont
  {{Sheng}}}, \bibinfo {author} {\bibfnamefont {O.~I.}\ \bibnamefont
  {{Motrunich}}}, \ and\ \bibinfo {author} {\bibfnamefont {M.~P.~A.}\
  \bibnamefont {{Fisher}}},\ }\href {\doibase 10.1038/nature11732} {\bibfield
  {journal} {\bibinfo  {journal} {\nat}\ }\textbf {\bibinfo {volume} {493}},\
  \bibinfo {pages} {39} (\bibinfo {year} {2013})},\ \Eprint
  {http://arxiv.org/abs/1207.6608} {arXiv:1207.6608 [cond-mat.str-el]}
  \BibitemShut {NoStop}%
\bibitem [{\citenamefont {Sur}\ and\ \citenamefont {Lee}(2014)}]{Shouvik1}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Sur}}\ and\ \bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont {Lee}},\
  }\href {\doibase 10.1103/PhysRevB.90.045121} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {90}},\ \bibinfo
  {pages} {045121} (\bibinfo {year} {2014})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Dalidovich}\ and\ \citenamefont
  {Lee}(2013)}]{Lee-Dalid}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {D.}~\bibnamefont
  {Dalidovich}}\ and\ \bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont
  {Lee}},\ }\href {\doibase 10.1103/PhysRevB.88.245106} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {88}},\
  \bibinfo {pages} {245106} (\bibinfo {year} {2013})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Mandal}\ and\ \citenamefont {Lee}(2015)}]{ips1}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {I.}~\bibnamefont
  {Mandal}}\ and\ \bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont
  {Lee}},\ }\href {\doibase 10.1103/PhysRevB.92.035141} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {92}},\
  \bibinfo {pages} {035141} (\bibinfo {year} {2015})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Sur}\ and\ \citenamefont {Lee}(2015)}]{shouvik2}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Sur}}\ and\ \bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont {Lee}},\
  }\href {\doibase 10.1103/PhysRevB.91.125136} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {91}},\ \bibinfo
  {pages} {125136} (\bibinfo {year} {2015})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Sur}}\ and\ \citenamefont {{Lee}}(2016)}]{sur16}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {{Sur}}}\ and\ \bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont
  {{Lee}}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal} {ArXiv
  e-prints}\ } (\bibinfo {year} {2016})},\ \Eprint
  {http://arxiv.org/abs/1606.06694} {arXiv:1606.06694 [cond-mat.str-el]}
  \BibitemShut {NoStop}%
\bibitem [{\citenamefont {Chung}\ \emph {et~al.}(2013)\citenamefont {Chung},
  \citenamefont {Mandal}, \citenamefont {Raghu},\ and\ \citenamefont
  {Chakravarty}}]{ips2}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {S.~B.}\ \bibnamefont
  {Chung}}, \bibinfo {author} {\bibfnamefont {I.}~\bibnamefont {Mandal}},
  \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont {Raghu}}, \ and\ \bibinfo
  {author} {\bibfnamefont {S.}~\bibnamefont {Chakravarty}},\ }\href {\doibase
  10.1103/PhysRevB.88.045127} {\bibfield  {journal} {\bibinfo  {journal} {Phys.
  Rev. B}\ }\textbf {\bibinfo {volume} {88}},\ \bibinfo {pages} {045127}
  (\bibinfo {year} {2013})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Eberlein}}\ \emph {et~al.}(2016)\citenamefont
  {{Eberlein}}, \citenamefont {{Mandal}},\ and\ \citenamefont
  {{Sachdev}}}]{ips4}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {{Eberlein}}}, \bibinfo {author} {\bibfnamefont {I.}~\bibnamefont
  {{Mandal}}}, \ and\ \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {{Sachdev}}},\ }\href@noop {} {\bibfield  {journal} {\bibinfo  {journal}
  {ArXiv e-prints}\ } (\bibinfo {year} {2016})},\ \Eprint
  {http://arxiv.org/abs/1605.00657} {arXiv:1605.00657 [cond-mat.str-el]}
  \BibitemShut {NoStop}%
\bibitem [{\citenamefont {Oganesyan}\ \emph {et~al.}(2001)\citenamefont
  {Oganesyan}, \citenamefont {Kivelson},\ and\ \citenamefont
  {Fradkin}}]{ogankivfr}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {V.}~\bibnamefont
  {Oganesyan}}, \bibinfo {author} {\bibfnamefont {S.~A.}\ \bibnamefont
  {Kivelson}}, \ and\ \bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Fradkin}},\ }\href {\doibase 10.1103/PhysRevB.64.195109} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {64}},\ \bibinfo {pages} {195109} (\bibinfo {year} {2001})}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Metzner}\ \emph {et~al.}(2003)\citenamefont
  {Metzner}, \citenamefont {Rohe},\ and\ \citenamefont
  {Andergassen}}]{metzner}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {W.}~\bibnamefont
  {Metzner}}, \bibinfo {author} {\bibfnamefont {D.}~\bibnamefont {Rohe}}, \
  and\ \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont {Andergassen}},\
  }\href {\doibase 10.1103/PhysRevLett.91.066402} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {91}},\
  \bibinfo {pages} {066402} (\bibinfo {year} {2003})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Dell'Anna}\ and\ \citenamefont
  {Metzner}(2006)}]{delanna}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {L.}~\bibnamefont
  {Dell'Anna}}\ and\ \bibinfo {author} {\bibfnamefont {W.}~\bibnamefont
  {Metzner}},\ }\href {\doibase 10.1103/PhysRevB.73.045127} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {73}},\ \bibinfo {pages} {045127} (\bibinfo {year} {2006})}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Kee}\ \emph {et~al.}(2003)\citenamefont {Kee},
  \citenamefont {Kim},\ and\ \citenamefont {Chung}}]{kee}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {H.-Y.}\ \bibnamefont
  {Kee}}, \bibinfo {author} {\bibfnamefont {E.~H.}\ \bibnamefont {Kim}}, \ and\
  \bibinfo {author} {\bibfnamefont {C.-H.}\ \bibnamefont {Chung}},\ }\href
  {\doibase 10.1103/PhysRevB.68.245109} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {68}},\ \bibinfo
  {pages} {245109} (\bibinfo {year} {2003})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Rech}\ \emph {et~al.}(2006)\citenamefont {Rech},
  \citenamefont {P\'epin},\ and\ \citenamefont {Chubukov}}]{rech}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {J.}~\bibnamefont
  {Rech}}, \bibinfo {author} {\bibfnamefont {C.}~\bibnamefont {P\'epin}}, \
  and\ \bibinfo {author} {\bibfnamefont {A.~V.}\ \bibnamefont {Chubukov}},\
  }\href {\doibase 10.1103/PhysRevB.74.195126} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {74}},\ \bibinfo
  {pages} {195126} (\bibinfo {year} {2006})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{W{\"o}lfle}}\ and\ \citenamefont
  {{Rosch}}(2007)}]{wolfle}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {P.}~\bibnamefont
  {{W{\"o}lfle}}}\ and\ \bibinfo {author} {\bibfnamefont {A.}~\bibnamefont
  {{Rosch}}},\ }\href {\doibase 10.1007/s10909-007-9308-y} {\bibfield
  {journal} {\bibinfo  {journal} {Journal of Low Temperature Physics}\ }\textbf
  {\bibinfo {volume} {147}},\ \bibinfo {pages} {165} (\bibinfo {year}
  {2007})},\ \Eprint {http://arxiv.org/abs/cond-mat/0609343} {cond-mat/0609343}
  \BibitemShut {NoStop}%
\bibitem [{\citenamefont {Maslov}\ and\ \citenamefont
  {Chubukov}(2010)}]{maslov}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {D.~L.}\ \bibnamefont
  {Maslov}}\ and\ \bibinfo {author} {\bibfnamefont {A.~V.}\ \bibnamefont
  {Chubukov}},\ }\href {\doibase 10.1103/PhysRevB.81.045110} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {81}},\ \bibinfo {pages} {045110} (\bibinfo {year} {2010})}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {{Quintanilla}}\ and\ \citenamefont
  {{Schofield}}(2006)}]{quintanilla}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {J.}~\bibnamefont
  {{Quintanilla}}}\ and\ \bibinfo {author} {\bibfnamefont {A.~J.}\ \bibnamefont
  {{Schofield}}},\ }\href {\doibase 10.1103/PhysRevB.74.115126} {\bibfield
  {journal} {\bibinfo  {journal} {\prb}\ }\textbf {\bibinfo {volume} {74}},\
  \bibinfo {eid} {115126} (\bibinfo {year} {2006})},\ \Eprint
  {http://arxiv.org/abs/cond-mat/0601103} {cond-mat/0601103} \BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {{Yamase}}\ and\ \citenamefont
  {{Kohno}}(2000)}]{yamase1}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {H.}~\bibnamefont
  {{Yamase}}}\ and\ \bibinfo {author} {\bibfnamefont {H.}~\bibnamefont
  {{Kohno}}},\ }\href {\doibase 10.1143/JPSJ.69.2151} {\bibfield  {journal}
  {\bibinfo  {journal} {Journal of the Physical Society of Japan}\ }\textbf
  {\bibinfo {volume} {69}},\ \bibinfo {pages} {2151} (\bibinfo {year}
  {2000})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Yamase}\ \emph {et~al.}(2005)\citenamefont {Yamase},
  \citenamefont {Oganesyan},\ and\ \citenamefont {Metzner}}]{yamase2}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {H.}~\bibnamefont
  {Yamase}}, \bibinfo {author} {\bibfnamefont {V.}~\bibnamefont {Oganesyan}}, \
  and\ \bibinfo {author} {\bibfnamefont {W.}~\bibnamefont {Metzner}},\ }\href
  {\doibase 10.1103/PhysRevB.72.035114} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {72}},\ \bibinfo
  {pages} {035114} (\bibinfo {year} {2005})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Halboth}\ and\ \citenamefont
  {Metzner}(2000)}]{halboth}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {C.~J.}\ \bibnamefont
  {Halboth}}\ and\ \bibinfo {author} {\bibfnamefont {W.}~\bibnamefont
  {Metzner}},\ }\href {\doibase 10.1103/PhysRevLett.85.5162} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo
  {volume} {85}},\ \bibinfo {pages} {5162} (\bibinfo {year}
  {2000})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Jakubczyk}\ \emph {et~al.}(2008)\citenamefont
  {Jakubczyk}, \citenamefont {Strack}, \citenamefont {Katanin},\ and\
  \citenamefont {Metzner}}]{jakub}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {P.}~\bibnamefont
  {Jakubczyk}}, \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {Strack}},
  \bibinfo {author} {\bibfnamefont {A.~A.}\ \bibnamefont {Katanin}}, \ and\
  \bibinfo {author} {\bibfnamefont {W.}~\bibnamefont {Metzner}},\ }\href
  {\doibase 10.1103/PhysRevB.77.195120} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {77}},\ \bibinfo
  {pages} {195120} (\bibinfo {year} {2008})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Zacharias}\ \emph {et~al.}(2009)\citenamefont
  {Zacharias}, \citenamefont {W\"olfle},\ and\ \citenamefont
  {Garst}}]{zacharias}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {M.}~\bibnamefont
  {Zacharias}}, \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {W\"olfle}},
  \ and\ \bibinfo {author} {\bibfnamefont {M.}~\bibnamefont {Garst}},\ }\href
  {\doibase 10.1103/PhysRevB.80.165116} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {80}},\ \bibinfo
  {pages} {165116} (\bibinfo {year} {2009})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Kim}\ \emph {et~al.}(2008)\citenamefont {Kim},
  \citenamefont {Lawler}, \citenamefont {Oreto}, \citenamefont {Sachdev},
  \citenamefont {Fradkin},\ and\ \citenamefont {Kivelson}}]{kim}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.-A.}\ \bibnamefont
  {Kim}}, \bibinfo {author} {\bibfnamefont {M.~J.}\ \bibnamefont {Lawler}},
  \bibinfo {author} {\bibfnamefont {P.}~\bibnamefont {Oreto}}, \bibinfo
  {author} {\bibfnamefont {S.}~\bibnamefont {Sachdev}}, \bibinfo {author}
  {\bibfnamefont {E.}~\bibnamefont {Fradkin}}, \ and\ \bibinfo {author}
  {\bibfnamefont {S.~A.}\ \bibnamefont {Kivelson}},\ }\href {\doibase
  10.1103/PhysRevB.77.184514} {\bibfield  {journal} {\bibinfo  {journal} {Phys.
  Rev. B}\ }\textbf {\bibinfo {volume} {77}},\ \bibinfo {pages} {184514}
  (\bibinfo {year} {2008})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Huh}}\ and\ \citenamefont {{Sachdev}}(2008)}]{huh}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {Y.}~\bibnamefont
  {{Huh}}}\ and\ \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {{Sachdev}}},\ }\href {\doibase 10.1103/PhysRevB.78.064512} {\bibfield
  {journal} {\bibinfo  {journal} {\prb}\ }\textbf {\bibinfo {volume} {78}},\
  \bibinfo {eid} {064512} (\bibinfo {year} {2008})},\ \Eprint
  {http://arxiv.org/abs/0806.0002} {arXiv:0806.0002 [cond-mat.str-el]}
  \BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Motrunich}}(2005)}]{MOTRUNICH}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {O.~I.}\ \bibnamefont
  {{Motrunich}}},\ }\href {\doibase 10.1103/PhysRevB.72.045105} {\bibfield
  {journal} {\bibinfo  {journal} {\prb}\ }\textbf {\bibinfo {volume} {72}},\
  \bibinfo {eid} {045105} (\bibinfo {year} {2005})},\ \Eprint
  {http://arxiv.org/abs/cond-mat/0412556} {cond-mat/0412556} \BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Lee}\ and\ \citenamefont {Lee}(2005)}]{LEE_U1}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont
  {Lee}}\ and\ \bibinfo {author} {\bibfnamefont {P.~A.}\ \bibnamefont {Lee}},\
  }\href {\doibase 10.1103/PhysRevLett.95.036403} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {95}},\
  \bibinfo {pages} {036403} (\bibinfo {year} {2005})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {{Lee}}\ \emph {et~al.}(2006)\citenamefont {{Lee}},
  \citenamefont {{Nagaosa}},\ and\ \citenamefont {{Wen}}}]{PALEE}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {P.~A.}\ \bibnamefont
  {{Lee}}}, \bibinfo {author} {\bibfnamefont {N.}~\bibnamefont {{Nagaosa}}}, \
  and\ \bibinfo {author} {\bibfnamefont {X.-G.}\ \bibnamefont {{Wen}}},\ }\href
  {\doibase 10.1103/RevModPhys.78.17} {\bibfield  {journal} {\bibinfo
  {journal} {Reviews of Modern Physics}\ }\textbf {\bibinfo {volume} {78}},\
  \bibinfo {pages} {17} (\bibinfo {year} {2006})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Motrunich}\ and\ \citenamefont
  {Fisher}(2007)}]{MotrunichFisher}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {O.~I.}\ \bibnamefont
  {Motrunich}}\ and\ \bibinfo {author} {\bibfnamefont {M.~P.~A.}\ \bibnamefont
  {Fisher}},\ }\href {\doibase 10.1103/PhysRevB.75.235116} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {75}},\ \bibinfo {pages} {235116} (\bibinfo {year} {2007})}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Yang}\ and\ \citenamefont {Sachdev}(2006)}]{subir6}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {K.}~\bibnamefont
  {Yang}}\ and\ \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont {Sachdev}},\
  }\href {\doibase 10.1103/PhysRevLett.96.187001} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo {volume} {96}},\
  \bibinfo {pages} {187001} (\bibinfo {year} {2006})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Xu}(2010)}]{xu10}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {C.}~\bibnamefont
  {Xu}},\ }\href {\doibase 10.1103/PhysRevB.81.054403} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {81}},\
  \bibinfo {pages} {054403} (\bibinfo {year} {2010})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Ruhman}\ and\ \citenamefont {Berg}(2014)}]{berg14}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {J.}~\bibnamefont
  {Ruhman}}\ and\ \bibinfo {author} {\bibfnamefont {E.}~\bibnamefont {Berg}},\
  }\href {\doibase 10.1103/PhysRevB.90.235119} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {90}},\ \bibinfo
  {pages} {235119} (\bibinfo {year} {2014})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Bahri}\ and\ \citenamefont
  {Potter}(2015)}]{potter15}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {Y.}~\bibnamefont
  {Bahri}}\ and\ \bibinfo {author} {\bibfnamefont {A.~C.}\ \bibnamefont
  {Potter}},\ }\href {\doibase 10.1103/PhysRevB.92.035131} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {92}},\ \bibinfo {pages} {035131} (\bibinfo {year} {2015})}\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Berg}\ \emph {et~al.}(2012)\citenamefont {Berg},
  \citenamefont {Rudner},\ and\ \citenamefont {Kivelson}}]{berg12}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {E.}~\bibnamefont
  {Berg}}, \bibinfo {author} {\bibfnamefont {M.~S.}\ \bibnamefont {Rudner}}, \
  and\ \bibinfo {author} {\bibfnamefont {S.~A.}\ \bibnamefont {Kivelson}},\
  }\href {\doibase 10.1103/PhysRevB.85.035116} {\bibfield  {journal} {\bibinfo
  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume} {85}},\ \bibinfo
  {pages} {035116} (\bibinfo {year} {2012})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Senthil}\ and\ \citenamefont
  {Shankar}(2009)}]{senshank}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {T.}~\bibnamefont
  {Senthil}}\ and\ \bibinfo {author} {\bibfnamefont {R.}~\bibnamefont
  {Shankar}},\ }\href {\doibase 10.1103/PhysRevLett.102.046406} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf {\bibinfo
  {volume} {102}},\ \bibinfo {pages} {046406} (\bibinfo {year}
  {2009})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Son}(1999)}]{son}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {D.~T.}\ \bibnamefont
  {Son}},\ }\href {\doibase 10.1103/PhysRevD.59.094019} {\bibfield  {journal}
  {\bibinfo  {journal} {Phys. Rev. D}\ }\textbf {\bibinfo {volume} {59}},\
  \bibinfo {pages} {094019} (\bibinfo {year} {1999})}\BibitemShut {NoStop}%
\bibitem [{\citenamefont {Mandal}\ and\ \citenamefont {Lee}()}]{ips-sc}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {I.}~\bibnamefont
  {Mandal}}\ and\ \bibinfo {author} {\bibfnamefont {S.-S.}\ \bibnamefont
  {Lee}},\ }\href@noop {} {\bibinfo  {journal} {To appear}\ }\BibitemShut
  {NoStop}%
\bibitem [{\citenamefont {Metlitski}\ \emph {et~al.}(2015)\citenamefont
  {Metlitski}, \citenamefont {Mross}, \citenamefont {Sachdev},\ and\
  \citenamefont {Senthil}}]{max}%
  \BibitemOpen
\bibfield  {journal} {  }\bibfield  {author} {\bibinfo {author} {\bibfnamefont
  {M.~A.}\ \bibnamefont {Metlitski}}, \bibinfo {author} {\bibfnamefont {D.~F.}\
  \bibnamefont {Mross}}, \bibinfo {author} {\bibfnamefont {S.}~\bibnamefont
  {Sachdev}}, \ and\ \bibinfo {author} {\bibfnamefont {T.}~\bibnamefont
  {Senthil}},\ }\href {\doibase 10.1103/PhysRevB.91.115111} {\bibfield
  {journal} {\bibinfo  {journal} {Phys. Rev. B}\ }\textbf {\bibinfo {volume}
  {91}},\ \bibinfo {pages} {115111} (\bibinfo {year} {2015})}\BibitemShut
  {NoStop}%
\end{thebibliography}%
