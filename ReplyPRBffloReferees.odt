We thank both referees for reading our manuscript and their helpful comments. Our detailed response is given below:


Response to the first referee
———————————————————

1.) Our effective action in Eq. (1) is a straightforward Fourier transform of the action in Eq. (4) of Ref. [1]. The only small difference is that we’ve included kinetic terms (i.e. k^2 \Delta(k)^2) for the order parameter fluctuations, since these terms are allowed by symmetry and will be generated by integrating out fermionic degrees of freedom in a renormalization group procedure anyways. This is a standard approach in effective field theories.

We emphasize that the form of our effective action is obtained using very well known techniques and can be easily rationalized by considering a standard Hamiltonian for a system of spinful electrons with mismatched Fermi surfaces (due to the Zeeman coupling to the magnetic field) and attractive electron-electron interactions. Performing a Hubbard-Stratonovich transformation in the pairing channel one straighforwardly obtains our effective action (without the kinetic term for the Hubbard-Stratonovich field \Delta, see above). This Hubbard-Stratonovich decoupling is a standard approach to derive a Ginzburg-Landau description of the phase transition after integrating out the fermionic degrees of freedom (also widely known as Hertz-Millis approach in the theory of metallic quantum critical points). Note that we do not integrate out the fermions here, but treat the fermionic and bosonic degrees of freedom on equal footing. This is crucial in two-dimensional systems, where integrating out the fermions is not permissible, because the strong interaction between bosons and fermions destroys the fermionic quasiparticle coherence. This is a well known, long standing problem in theoretical condensed matter physics, see e.g. the Sec. 18 in the book „Quantum Phase transitions“ (2nd edition) by S. Sachdev for an introduction.

Also note that the „mass“ term ~ m \Delta^2 of the boson (i.e. the order parameter field) plays the role of the magnetic field and is used to tune through the quantum phase transition from the normal metal to the FFLO state. This is in precise analogy to classical Ginzburg-Landau theory, where the mass-term is proportional to the temperature away from the critical point m ~ (T/Tc - 1).

We added a detailed discussion of the rationale behind our Eq. (1) in the revised version of our manuscript and hope this clarifies this issue.

2.) We emphasize that our action does not describe the phase transition between the FFLO phase and the „balanced“ BCS superconducting phase. In fact, it is only valid in the vicinity of the quantum critical point between the FFLO phase and the normal metal (see Fig. 1 in our paper), where the relevant low energy degrees of freedom are the FFLO order parameter fluctuations as well as the electronic quasiparticle excitations at the Fermi surface, which are described in full generality by our model. Our effective field theory can be straightforwardly obtained from a model of spinful fermions with mismatched Fermi surfaces and an attractive electron-electron interaction. We emphasize that the FFLO to normal-metal transition in 2D is not of first order, as we show explicitly in our work (and in accordance with most previous theoretical work on this subject). It is important to note that most theoretical approaches so far rely on mean-field type techniques, which are not trustworthy in two-dimensions. In fact, our renormalization group calculation is the first controlled, analytic computation which shows that the transition is continuous (i.e. 2nd order) in two dimensional systems.

3.) We thank the referee for pointing this out. Indeed, our specific predictions for experiments were a little buried in the main text and in the conclusions. In order to remedy this problem, we added to new sections: A section VI in which physical observables are discussed, and a section II at the beginning of the manuscript which summarizes all our finding and predictions. 



Reply to the second referee
——————————————————

We thank the referee for his positive review. We agree that our main results and predictions were not easy to find in this rather long manuscript. To make our work better readable and highlight the advantages of our RG approach, we have added two new sections: A new section II beginning of the manuscript, where all results and predictions from our calculation are summarized, and a section VI in which physical observables are discussed. 

 


List of Changes
——————————————————
 
- We added a new section (section II) which summarizes our general approach, the critical exponents, and in particular the physical/experimental implications of our findings. In addition to the physical observables which can be readily read off from the scaling form of the critical correlations or were computed in Ref. [1], we included a prediction for the scaling behaviour of the magnetic susceptibility. The corresponding computation is presented in the new Appendix E.

- At the end of secion V. A, we included references which support our claim of a second order phase transition.

- In section V. B, we have slightly modified our argument in favour of \nu \simeq. (Eq. 44)

- In the main text we included a new section (section VI), where physical observables are discussed in more detail. In particular, this new section now contains the discussion of thermodynamic properties which was previously part of the conclusion.

- We have streamlined our conclusion (sec. VIII), including another suggestion for further study (role of disorder).