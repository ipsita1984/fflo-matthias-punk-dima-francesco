(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     35490,       1004]
NotebookOptionsPosition[     32107,        912]
NotebookOutlinePosition[     32441,        927]
CellTagsIndexPosition[     32398,        924]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.703951966609188*^9, 
  3.703952001609585*^9}},ExpressionUUID->"99fad2fb-71ea-466d-9de2-\
5682f42b3bbf"],

Cell[CellGroupData[{

Cell["Bose Self-energy", "Chapter",
 CellChangeTimes->{{3.704010310505155*^9, 3.7040103114491243`*^9}, {
  3.718615687357723*^9, 
  3.718615690853677*^9}},ExpressionUUID->"47f4cdb3-e3a9-4a97-8ea1-\
a6696ac927de"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"M1", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}]}], "}"}]}], ";", " ", 
    RowBox[{"M2", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "1"}], "}"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{"MatrixForm", "[", "M1", "]"}], " ", "\n", 
   RowBox[{"NewTrace1", "=", 
    RowBox[{
     RowBox[{"Table", "[", " ", 
      RowBox[{
       RowBox[{"Tr", "[", 
        RowBox[{
         RowBox[{"PauliMatrix", "[", "i", "]"}], ".", "M1", ".", 
         RowBox[{"PauliMatrix", "[", "j", "]"}], ".", "M2"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "1", ",", "3"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j", ",", "1", ",", "3"}], "}"}]}], "]"}], "//", 
     "MatrixForm"}]}]}]}]], "Input",
 CellChangeTimes->{{3.7186160704042063`*^9, 3.718616108860689*^9}, {
  3.7186173017019463`*^9, 
  3.718617302016527*^9}},ExpressionUUID->"3f99533c-50ce-4afb-a072-\
6ca4bdd629c9"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1", "0"},
     {"0", "0"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.7186173034420223`*^9},ExpressionUUID->"991ed0e4-cb3b-400c-905d-\
031bd67b4722"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1", 
      RowBox[{"-", "\[ImaginaryI]"}], "0"},
     {"\[ImaginaryI]", "1", "0"},
     {"0", "0", "0"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.718617303448661*^9},ExpressionUUID->"cfaa252c-6e2c-4dbd-b26e-\
4a851c7fb4cf"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Tr", "[", 
  RowBox[{
   RowBox[{"PauliMatrix", "[", "2", "]"}], ".", "M1", ".", 
   RowBox[{"PauliMatrix", "[", "1", "]"}], ".", "M2"}], "]"}]], "Input",
 CellChangeTimes->{{3.718617305568186*^9, 3.71861731946873*^9}, {
  3.7186173981706133`*^9, 
  3.718617409703966*^9}},ExpressionUUID->"11fd3188-3fd3-4387-8dbe-\
0d77fecc32ea"],

Cell[BoxData["\[ImaginaryI]"], "Output",
 CellChangeTimes->{{3.7186173146434097`*^9, 3.718617320053853*^9}, 
   3.718617410776785*^9},ExpressionUUID->"d4170342-1420-4879-839a-\
deaaf27d610f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"x", "\[Equal]", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "v1"}], "*", "pd1"}], "+", 
      RowBox[{"pd", "^", "2"}]}]}], "&&", 
    RowBox[{"y", "\[Equal]", 
     RowBox[{
      RowBox[{"v2", "*", 
       RowBox[{"(", 
        RowBox[{"kd1", "-", "pd1"}], ")"}]}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"kd", "-", "pd"}], ")"}], "^", "2"}]}]}]}], ",", 
   RowBox[{"{", 
    RowBox[{"pd", ",", "pd1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.701780110199733*^9, 3.70178028330584*^9}, {
  3.701780362455083*^9, 
  3.701780399261428*^9}},ExpressionUUID->"6713f908-7971-4fe5-b348-\
1b7fbc5293e6"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"pd", "\[Rule]", 
      FractionBox[
       RowBox[{
        RowBox[{"2", " ", "kd", " ", "v1"}], "-", 
        SqrtBox[
         RowBox[{
          RowBox[{"4", " ", 
           SuperscriptBox["kd", "2"], " ", 
           SuperscriptBox["v1", "2"]}], "-", 
          RowBox[{"4", " ", 
           RowBox[{"(", 
            RowBox[{"v1", "-", "v2"}], ")"}], " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              SuperscriptBox["kd", "2"], " ", "v1"}], "+", 
             RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
             RowBox[{"v2", " ", "x"}], "-", 
             RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}], 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{"v1", "-", "v2"}], ")"}]}]]}], ",", 
     RowBox[{"pd1", "\[Rule]", 
      FractionBox[
       RowBox[{
        RowBox[{"-", 
         SuperscriptBox["kd", "2"]}], "+", 
        FractionBox[
         RowBox[{"2", " ", 
          SuperscriptBox["kd", "2"], " ", "v1"}], 
         RowBox[{"v1", "-", "v2"}]], "-", 
        RowBox[{"kd1", " ", "v2"}], "-", "x", "+", "y", "-", 
        FractionBox[
         RowBox[{"kd", " ", 
          SqrtBox[
           RowBox[{
            RowBox[{"4", " ", 
             SuperscriptBox["kd", "2"], " ", 
             SuperscriptBox["v1", "2"]}], "-", 
            RowBox[{"4", " ", 
             RowBox[{"(", 
              RowBox[{"v1", "-", "v2"}], ")"}], " ", 
             RowBox[{"(", 
              RowBox[{
               RowBox[{
                SuperscriptBox["kd", "2"], " ", "v1"}], "+", 
               RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
               RowBox[{"v2", " ", "x"}], "-", 
               RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}], 
         RowBox[{"v1", "-", "v2"}]]}], 
       RowBox[{"v1", "-", "v2"}]]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"pd", "\[Rule]", 
      FractionBox[
       RowBox[{
        RowBox[{"2", " ", "kd", " ", "v1"}], "+", 
        SqrtBox[
         RowBox[{
          RowBox[{"4", " ", 
           SuperscriptBox["kd", "2"], " ", 
           SuperscriptBox["v1", "2"]}], "-", 
          RowBox[{"4", " ", 
           RowBox[{"(", 
            RowBox[{"v1", "-", "v2"}], ")"}], " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              SuperscriptBox["kd", "2"], " ", "v1"}], "+", 
             RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
             RowBox[{"v2", " ", "x"}], "-", 
             RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}], 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{"v1", "-", "v2"}], ")"}]}]]}], ",", 
     RowBox[{"pd1", "\[Rule]", 
      FractionBox[
       RowBox[{
        RowBox[{"-", 
         SuperscriptBox["kd", "2"]}], "+", 
        FractionBox[
         RowBox[{"2", " ", 
          SuperscriptBox["kd", "2"], " ", "v1"}], 
         RowBox[{"v1", "-", "v2"}]], "-", 
        RowBox[{"kd1", " ", "v2"}], "-", "x", "+", "y", "+", 
        FractionBox[
         RowBox[{"kd", " ", 
          SqrtBox[
           RowBox[{
            RowBox[{"4", " ", 
             SuperscriptBox["kd", "2"], " ", 
             SuperscriptBox["v1", "2"]}], "-", 
            RowBox[{"4", " ", 
             RowBox[{"(", 
              RowBox[{"v1", "-", "v2"}], ")"}], " ", 
             RowBox[{"(", 
              RowBox[{
               RowBox[{
                SuperscriptBox["kd", "2"], " ", "v1"}], "+", 
               RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
               RowBox[{"v2", " ", "x"}], "-", 
               RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}], 
         RowBox[{"v1", "-", "v2"}]]}], 
       RowBox[{"v1", "-", "v2"}]]}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.7017801718013363`*^9, 3.701780243414328*^9}, 
   3.701780399885096*^9},ExpressionUUID->"e3d16413-8712-4653-8670-\
c5897d79d8af"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Assuming", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"v1", ">", "0"}], "&&", 
    RowBox[{"v2", ">", "0"}], "&&", 
    RowBox[{"Element", "[", 
     RowBox[{"kd", ",", "Reals"}], "]"}], "&&", 
    RowBox[{"Element", "[", 
     RowBox[{"kd1", ",", "Reals"}], "]"}]}], ",", " ", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{
     RowBox[{"2", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"v1", "*", 
         RowBox[{"(", 
          RowBox[{"kd", "-", "pd"}], ")"}]}], "+", 
        RowBox[{"pd", "*", "v2"}]}], ")"}]}], "/.", 
     RowBox[{"{", 
      RowBox[{"pd", "\[Rule]", " ", 
       FractionBox[
        RowBox[{
         RowBox[{"2", " ", "kd", " ", "v1"}], "+", 
         SqrtBox[
          RowBox[{
           RowBox[{"4", " ", 
            SuperscriptBox["kd", "2"], " ", 
            SuperscriptBox["v1", "2"]}], "-", 
           RowBox[{"4", " ", 
            RowBox[{"(", 
             RowBox[{"v1", "-", "v2"}], ")"}], " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{
               SuperscriptBox["kd", "2"], " ", "v1"}], "+", 
              RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
              RowBox[{"v2", " ", "x"}], "-", 
              RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}], 
        RowBox[{"2", " ", 
         RowBox[{"(", 
          RowBox[{"v1", "-", "v2"}], ")"}]}]]}], "}"}]}], "]"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.701780527093183*^9, 3.7017805639486437`*^9}, {
  3.7017806110841846`*^9, 3.701780703465459*^9}, {3.7017809408159647`*^9, 
  3.701780941158845*^9}, {3.701780996213293*^9, 3.701781010676094*^9}, {
  3.701869182890141*^9, 
  3.7018691843617887`*^9}},ExpressionUUID->"5b120867-c89d-40e4-9fcc-\
8fe3086bad8b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "2"}], " ", 
  SqrtBox[
   RowBox[{
    RowBox[{
     SuperscriptBox["kd", "2"], " ", "v1", " ", "v2"}], "-", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"v1", "-", "v2"}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
       RowBox[{"v2", " ", "x"}], "-", 
       RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}]], "Output",
 CellChangeTimes->{{3.7017806934298973`*^9, 3.701780705243972*^9}, 
   3.7017809428116207`*^9, 
   3.701781014146414*^9},ExpressionUUID->"ec6b99d9-50a7-43b8-940e-\
5fb4be631688"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Assuming", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"v1", ">", "0"}], "&&", 
    RowBox[{"v2", ">", "0"}], "&&", 
    RowBox[{"Element", "[", 
     RowBox[{"kd", ",", "Reals"}], "]"}], "&&", 
    RowBox[{"Element", "[", 
     RowBox[{"kd1", ",", "Reals"}], "]"}]}], ",", " ", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{
     RowBox[{"2", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"v1", 
         RowBox[{"(", 
          RowBox[{"kd", "-", "pd"}], ")"}]}], "+", 
        RowBox[{"pd", "*", "v2"}]}], ")"}]}], "/.", 
     RowBox[{"{", 
      RowBox[{"pd", "\[Rule]", " ", 
       FractionBox[
        RowBox[{
         RowBox[{"2", " ", "kd", " ", "v1"}], "-", 
         SqrtBox[
          RowBox[{
           RowBox[{"4", " ", 
            SuperscriptBox["kd", "2"], " ", 
            SuperscriptBox["v1", "2"]}], "-", 
           RowBox[{"4", " ", 
            RowBox[{"(", 
             RowBox[{"v1", "-", "v2"}], ")"}], " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{
               SuperscriptBox["kd", "2"], " ", "v1"}], "+", 
              RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
              RowBox[{"v2", " ", "x"}], "-", 
              RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}], 
        RowBox[{"2", " ", 
         RowBox[{"(", 
          RowBox[{"v1", "-", "v2"}], ")"}]}]]}], "}"}]}], "]"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.7017807415030537`*^9, 3.701780756503957*^9}, {
  3.701780946655039*^9, 3.701780946998214*^9}, {3.701780985893445*^9, 
  3.701780987452673*^9}},ExpressionUUID->"3efae383-aae3-4bcf-ae61-\
18df831a81a8"],

Cell[BoxData[
 RowBox[{"2", " ", 
  SqrtBox[
   RowBox[{
    RowBox[{
     SuperscriptBox["kd", "2"], " ", "v1", " ", "v2"}], "-", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"v1", "-", "v2"}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"kd1", " ", "v1", " ", "v2"}], "+", 
       RowBox[{"v2", " ", "x"}], "-", 
       RowBox[{"v1", " ", "y"}]}], ")"}]}]}]]}]], "Output",
 CellChangeTimes->{3.701780760616206*^9, 3.701780948275008*^9, 
  3.7017809887621603`*^9},ExpressionUUID->"2265118d-5484-45f4-9834-\
0643761e2391"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Assuming", "[", 
  RowBox[{
   RowBox[{"B", ">", "0"}], ",", " ", 
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"y", "^", "2"}], "+", "B"}], ")"}]}], "*", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"2", "Pi"}], ")"}]}]}], ",", 
     RowBox[{"{", 
      RowBox[{"y", ",", 
       RowBox[{"-", "Infinity"}], ",", "Infinity"}], "}"}]}], "]"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.704012212422984*^9, 3.704012259736135*^9}, 
   3.72077116280682*^9},ExpressionUUID->"0881ca8d-11ba-4da3-9144-\
88f3d49034c3"],

Cell[BoxData[
 FractionBox["1", 
  RowBox[{"2", " ", 
   SqrtBox["B"]}]]], "Output",
 CellChangeTimes->{
  3.7040122608434772`*^9},ExpressionUUID->"fa9686e1-5122-4bde-a54c-\
fa0ce7c0226a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Assuming", "[", 
  RowBox[{
   RowBox[{"2", "<", "d", "<", "3"}], ",", " ", 
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"t", "*", 
        RowBox[{"(", 
         RowBox[{"1", "-", "t"}], ")"}]}], ")"}], "^", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"d", "/", "2"}], "-", "1"}], ")"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"t", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.704015566239962*^9, 
  3.704015600796452*^9}},ExpressionUUID->"b1adaaa5-f965-42a8-80f8-\
f9de652dc4cf"],

Cell[BoxData[
 FractionBox[
  SuperscriptBox[
   RowBox[{"Gamma", "[", 
    FractionBox["d", "2"], "]"}], "2"], 
  RowBox[{"Gamma", "[", "d", "]"}]]], "Output",
 CellChangeTimes->{
  3.70401560197221*^9},ExpressionUUID->"ace4ec19-c0e4-4770-b354-3d1ad368113b"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Assuming", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"p", ">", "0"}], "&&", 
    RowBox[{"Element", "[", 
     RowBox[{"k0", ",", "Reals"}], "]"}], "&&", 
    RowBox[{"Element", "[", 
     RowBox[{"k", ",", "Reals"}], "]"}], "&&", 
    RowBox[{"2", "<", "d", "<", "3"}]}], ",", " ", 
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"2", "Pi"}], ")"}]}], "*", 
      RowBox[{"p", "^", 
       RowBox[{"(", 
        RowBox[{"d", "-", "3"}], ")"}]}], "*", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"p0", "^", "2"}], "-", 
           RowBox[{"k0", "^", "2"}]}], ")"}], "/", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"p", "^", "2"}], "+", 
           RowBox[{"p0", "^", "2"}], "+", 
           RowBox[{"k", "^", "2"}], "+", 
           RowBox[{"k0", "^", "2"}]}], ")"}]}], " ", "-", " ", 
        RowBox[{
         RowBox[{"p0", "^", "2"}], "/", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"p", "^", "2"}], "+", 
           RowBox[{"p0", "^", "2"}]}], ")"}]}]}], ")"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"p0", ",", 
       RowBox[{"-", "Infinity"}], ",", "Infinity"}], "}"}]}], "]"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.704015825503934*^9, 3.704015926655005*^9}, {
  3.70401595804935*^9, 3.7040159616136827`*^9}, {3.704016104984572*^9, 
  3.704016142310534*^9}},ExpressionUUID->"0dd3b153-d34c-4f84-89a8-\
e524e7104289"],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{
    SuperscriptBox["p", 
     RowBox[{
      RowBox[{"-", "3"}], "+", "d"}]], " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["k", "2"], "+", 
      RowBox[{"2", " ", 
       SuperscriptBox["k0", "2"]}], "+", 
      RowBox[{"p", " ", 
       RowBox[{"(", 
        RowBox[{"p", "-", 
         SqrtBox[
          RowBox[{
           SuperscriptBox["k", "2"], "+", 
           SuperscriptBox["k0", "2"], "+", 
           SuperscriptBox["p", "2"]}]]}], ")"}]}]}], ")"}]}], 
   RowBox[{"2", " ", 
    SqrtBox[
     RowBox[{
      SuperscriptBox["k", "2"], "+", 
      SuperscriptBox["k0", "2"], "+", 
      SuperscriptBox["p", "2"]}]]}]]}]], "Output",
 CellChangeTimes->{3.704015940813138*^9, 3.704016017918461*^9, 
  3.7040161153721237`*^9, 
  3.704016145639763*^9},ExpressionUUID->"9f2c7114-cb94-4912-8889-\
713f236f65b9"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Assuming", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"Element", "[", 
     RowBox[{"k0", ",", "Reals"}], "]"}], "&&", 
    RowBox[{"Element", "[", 
     RowBox[{"k", ",", "Reals"}], "]"}], "&&", 
    RowBox[{"2", "<", "d", "<", "3"}]}], ",", 
   RowBox[{"Integrate", "[", " ", 
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        SuperscriptBox["p", 
         RowBox[{
          RowBox[{"-", "3"}], "+", "d"}]], " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["k", "2"], "+", 
          RowBox[{"2", " ", 
           SuperscriptBox["k0", "2"]}], "+", 
          RowBox[{"p", " ", 
           RowBox[{"(", 
            RowBox[{"p", "-", 
             SqrtBox[
              RowBox[{
               SuperscriptBox["k", "2"], "+", 
               SuperscriptBox["k0", "2"], "+", 
               SuperscriptBox["p", "2"]}]]}], ")"}]}]}], ")"}]}], 
       RowBox[{"2", " ", 
        SqrtBox[
         RowBox[{
          SuperscriptBox["k", "2"], "+", 
          SuperscriptBox["k0", "2"], "+", 
          SuperscriptBox["p", "2"]}]]}]]}], ",", 
     RowBox[{"{", 
      RowBox[{"p", ",", "0", ",", "Infinity"}], "}"}]}], "]"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.704016167428256*^9, 
  3.70401619232364*^9}},ExpressionUUID->"f1cf41a7-5c8e-4599-90dd-\
4fd3029174f5"],

Cell[BoxData[
 RowBox[{"ConditionalExpression", "[", 
  RowBox[{
   FractionBox[
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["k", "2"], "+", 
        SuperscriptBox["k0", "2"]}], ")"}], 
      RowBox[{
       FractionBox["1", "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "3"}], "+", "d"}], ")"}]}]], " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["k", "2"], "+", 
       RowBox[{"d", " ", 
        SuperscriptBox["k0", "2"]}]}], ")"}], " ", 
     RowBox[{"Gamma", "[", 
      RowBox[{
       FractionBox["1", "2"], "-", 
       FractionBox["d", "2"]}], "]"}], " ", 
     RowBox[{"Gamma", "[", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       FractionBox["d", "2"]}], "]"}]}], 
    RowBox[{"8", " ", 
     SqrtBox["\[Pi]"]}]], ",", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"k", "\[NotEqual]", "0"}], "||", 
      RowBox[{"k0", "\[NotEqual]", "0"}]}], ")"}], "&&", 
    RowBox[{
     SqrtBox[
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["k", "2"]}], "-", 
       SuperscriptBox["k0", "2"]}]], "\[NotEqual]", "0"}]}]}], 
  "]"}]], "Output",
 CellChangeTimes->{
  3.70401625415663*^9},ExpressionUUID->"a1211f71-ea8c-49ec-a12d-76c003c836a4"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"\[Chi]d", "[", "d_", "]"}], ":=", " ", 
    RowBox[{
     FractionBox[
      RowBox[{" ", 
       RowBox[{"Gamma", "[", 
        RowBox[{
         FractionBox["1", "2"], "-", 
         FractionBox["d", "2"]}], "]"}], " "}], 
      RowBox[{"8", " ", 
       SqrtBox["\[Pi]"]}]], "*", 
     RowBox[{
      RowBox[{"2", "^", 
       RowBox[{"(", 
        RowBox[{"2", "-", "d"}], ")"}]}], "/", 
      RowBox[{"(", 
       RowBox[{"Pi", "^", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"(", "d", ")"}], "/", "2"}], ")"}]}], ")"}]}], "*", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"Gamma", "[", 
          RowBox[{"d", "/", "2"}], "]"}], "^", "2"}], "/", 
        RowBox[{"Gamma", "[", "d", "]"}]}], ")"}], "/", "2"}]}]}], ";"}], 
  " "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Chi]d", "[", 
   RowBox[{"5", "/", "2"}], "]"}], "//", "N"}]}], "Input",
 CellChangeTimes->{{3.704016728419911*^9, 3.704016846143898*^9}, {
   3.704016893984435*^9, 3.704016931500215*^9}, {3.704108959191297*^9, 
   3.704108986551147*^9}, {3.704109914009141*^9, 3.704109919239704*^9}, {
   3.71550195710194*^9, 3.715501957197095*^9}, {3.718617831476424*^9, 
   3.718617831622916*^9}, 
   3.7186286827329063`*^9},ExpressionUUID->"4dd6905b-d86a-4fad-b7e9-\
5162561dff9a"],

Cell[BoxData[
 RowBox[{"-", "0.017810598831381322`"}]], "Output",
 CellChangeTimes->{{3.704016829770426*^9, 3.704016847521009*^9}, {
   3.7040169031209793`*^9, 3.704016932105246*^9}, 3.704108990927569*^9, 
   3.704109925976551*^9, 3.71550198486199*^9, 3.718617833159172*^9, 
   3.71862868352745*^9},ExpressionUUID->"22775022-73f5-4f3b-802c-\
a66ac6a73ddc"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[Chi]d", "[", "2", "]"}]], "Input",
 CellChangeTimes->{{3.715502007307714*^9, 
  3.715502012211068*^9}},ExpressionUUID->"ce7fc89e-eca0-4e9b-891b-\
cfaee2c1d978"],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox["1", 
   RowBox[{"8", " ", "\[Pi]"}]]}]], "Output",
 CellChangeTimes->{3.7155020128347597`*^9, 3.7186178374597387`*^9, 
  3.7186287690844727`*^9},ExpressionUUID->"3db690c0-992c-4b10-9a45-\
c38c1c6d437f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"\[Chi]d", "[", "d", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"d", ",", "2", ",", "3"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7040169948074408`*^9, 3.7040170041212063`*^9}, {
  3.704109921671908*^9, 
  3.704109923711464*^9}},ExpressionUUID->"d57ff73a-d8ed-4c19-8029-\
99717c5cb4ca"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwV13k8FV8bAPCLmbm2rGXJFmlRSMivUjyItKmkUpSoJJRU1iQkRKq5176v
kSUiUqpjy561UEhZEyVLhaj3vH/dz/dzZ86c88xznueMvI2T6VlOBoNxlYPB
+P/v8s0v//8DdS3dJ9Ss0lCbz5+mJGz71Qolty3SUHDdllg9Dgb0epgyB4+m
ofnjxRo3ORmws+3gzdgDaej99bwzFMmAD8K/Rpfpp6GoyuRqfl4GaPzw0di8
Kg0t2x8QLLmUAXPRB+vVxlORkO1+EY21DIjrrJ7o90pFZ269qqg5xIAbZdmp
iZEpaJ55WcbDiwH8Rk/dwpqTUeDlnXIn0xhAq3SoFIgnI+emufK0ZgacLX4U
89cyCSWE/hDgnmWAjj7vfwNXEtFanaqFxpUcsCLGKBCUElBmxzP6tQkHWKfG
pG+Yj0MObdMtH1w5IHX06lG/yVgUgngfH0zhgBFGU+zMYAz6LmirPVTPAS7p
Rov35qPRTuMBx5wpDjiikf9Oa2U0+uu8XfedDCdETgv6fzCOQr//dpcuN+KE
cv9drSXBkcjk+tH35c6cEOXL6fG8PgLFDszEx8Vxgt4xuVtdUhFoXW4Hf1gV
Jwy9/Cl3wj8ciduFS3+bwPd73f2zbTwMGQVEddhIcMGw+LrDxufDUHu4+8at
O7hA8qbyc94RNjoS6qlh6MQFwhPOMbmX2UjqS1mfYyQXLBXfJaklxEY6moPK
vyq4gNGS/flwPgulDuxRjBvjgrhDg62c1iz0z/NdhYsoATGTRhkfpFlIYVl7
nsJSAjg3fRaqlWKhHXktcc3YKef01YqXs1DwQIOrkhgB2+9G3Q+TYCGxvRXr
eiUIGDE2e39sKQupyuSxdsgSIFPlG03xs5AlCrIRVSLA9+LQ/tE/NCrh2sb5
WIeAUi3u0c5eGtnl9Vub6hLwyeBaBCe2hMXt8mls0ZC0YZUeGrkXdPho6RGQ
SDk6BXyg0WZr53+lBgQwLZat0u2k0dNXDxaqjQnYn+qk0dxCo2IP4d89pgTc
GOh1s6+ike2qkiPehwgo6dr/4GEljcRaTxbLmRGg/nWn52gFjVzX5rjYHCbg
vbSKqEM5jbQ6jWZGjhJgZh/A5/qKRkWaXpNTlng8jRfK2SU0ejIxPMZ9joCe
V0++snJptE5QuO0ptrPRz5iuHBolqmqX2NoRsC4zQlgOO/jCPf+q8wSE5t43
fZRFI6ux/2R8HPH6hLzXdGTQiHckyOS3MwEi7m909qTQ6NTHdY+HvAiQ+1a6
tTGSRh2LZpFh1wmIesLHtR57j8yN6wbeBIxBbURwBJ6vZfuu5BsEvMr9T35v
OI34Plzrt/QjwC+psbqLhePxrlH0bSAB9olXvSTu0mjJm4uuFSwCDO/Ld6vf
opHC1U3rL7IJqFUq+5zrj+MrtdAnGUZAu4pSgRK2zfnbu5zD8fh5Mo9X3sTj
kWnSK6II4NebUZb3xfPVeV9xPZ6AP2+2Gutfp5HLUKKbUgIBK2/0ctZ74fXf
sVV+hy0+djnYFLvow3S4chK+/rIxh+01vH43gfPdKQS0BF9gR3jg+OYZCG7J
JCDVYMxd1pVGdUd4qwax4b8vUoUuNPq42OJ+/yHOv06de8bYPHtP9o9kEfDs
VfeIy1UanfziXhSRS0DWxmXnPlymEbf8I4uZAgK+8zq21DvRyJIlkZn3koAO
49/Tu+1p9Hd3UPP+VwQ0bhe68e88jZK4Zn9NYK9fzlX9BHvoaqehWhkBb7p2
B8ljXzwaMZBfQYD8n22DXHY08pFeKldQQ0DBF22l8bM4nu9uGpnWEuBauTsk
B7sqdPrCFLb9y4ScC9g8/9peqNcTcK5rVmXqDI1Yn+njhY0EeOoVbySw0x8I
RjxpJUB4bsWlvTY0Mra68dKsjQCBbazPEthfxScGZ7Drt0wsG7amkertJo1N
bwm4df1krR92iUNoa1EHAffTXx1/fYpGxxUXZw93EpAWuPlrJPZCj+OKX9jq
tUE6Dth6JnudtN4TIO3xQGspdr0a35Kn3QT8OFvq4GRFI8dRT82jPQSM7mhY
a4wtkPLV4jd2hElOjjz2IdH6rP8+ErBKdZCn8ySNen4GGZd8wvncvUt+D7b3
o1kn888EZC/8k1qHveKcXeQsNruY1ceDfbZr5/DmAZxPReYVDSdo9P055f9s
iICXuuWL5thHRuY2nhsmYDDeQWAb9ivRb31LRwgoE2ldsgI71LFd+9IXAjJq
lD98taTRTFT1qMwoAf5l7Rmt2Javn0U2YN/Qjzz7DFtZNml69RgBquz3BSHY
YbvZyW+x32jwG7hiL7gG7PcbJ0CLs7LKGruh2TG79xuujzJKCdrY6gtWx0K+
E3DT5t4vJeyYtYeYWyYI6H09oieJbe+75XTYD5wf2p8L5i1o1JarLKw/iePr
vu39OPbWD3JoAttv3d/pPuwUSvRC/BQBax38ud5i82hQUnumcT0pVeGuw75k
NVc7i51CHuNE2F0h464PZgg4EX92qggbSvoUzX7i+EFhVy525mBbG8cvAsyX
pRQ9wBYSrvbJww7IY91Ownbf/kz1xG8C/vINm8Vifzqf08M7i/cLe1E8Entn
RGJwCbablHY7G/tRBWuz7RwBTwYZgTS22MStYdF5AuLjH2vcx/aW8ggrxy6x
zn9/D3t4p6O+0x8Cjjw/5fH//02uWv2QXiCAK3G1KAu7OMk0oR573sU2Iwxb
9o3hXvdFnB9Lb2lFYd+a2zy/6i/On7KRsjjs8VXKme3YRiozRinYZqZyR3z/
ETDlK1SXif3CW4TYwCABmaXuzMdWzCYLerC7ckUrS7DvdM5aBXOQcJJZp12B
PcM1vmQzJwlf9q0vaMS2VOsrHcKe/5Syugu70rLtPJuLBB92VPQg9rrbr8X1
CBIO5LjwTWGzikpef8eOHU71YuD3bSOQKL+bIsHpZ9zJFdj1W1nNv7FfB29v
2fj/fDl363o6kwSNqg/6O7A5yhzeM3hI4H9ZquyIbTd+MvARtt5MV5ovdouE
6SZLXhKUy9LlorCTnDfTT/lIWKZJydVicyes1z3LT8JqKbe0z9hO9bLfRJaQ
ICNmqrqArbOS3HVRgAQ+PuqgJt4fvW9bGauE8f0O6GM+dpvIRfkabA3egn0t
2NUHePXPi5BwbDw55Qd2fqP+zVxREpqMpv5p4P3pX1VIaomRIKFbwnqN7cF5
YHUXdoDuT+1x7IswbuQpTsLLJeubRHE9OPpCMeiVBF6/GpV3Fnvdk3DenVIk
VJ/SvPD/+iI3tXH9KLbZyHc7feylak17QqTx/c+LDzhjL2ZToc0yJBTLMIZb
sVtS3QTNV5Cwxz5UPQHXN1f2saXnV5FQcz0o9/RpGjm0/tTkW01Cym7umCTs
U4Ksw7nYOVH8Hh+xd4XUR0yuIcF3136hE7i+St/UlvRcR0KmQWjeWVyPKy7L
yIZsIOGqXp9Q+jl8nsh/pqOiRsKlgFrzSeyc74etmrF7nkvF6OB6H25/N0lU
nYRg8TreHmw7m38KcZokvHnwPl8B9xNB089rcreQQOaruPY50ihNfLn43q04
H+sDS7ZcwP2115Qaw+Y3TJ4Mw7axqxpcu40EwcreXSYXcf/zzkhO0yGBuLM6
sRH3q2PZF6TiDEjQ8m4wmMX97PulB7zaO0g4zfdlrc0VGvlp9c29x+6rW8P/
Bjun/ECXuBEJ7bfmm9JxP/zbqRHONiahfIPjodO4n6YS8wIh+0iYKpd+LeCJ
51Ov/lfJBMe78V11EHbjPYdvtdhnCllNXLg//1re20AdIIF7rcLUArbxxrIg
P1MSJkv/RP7B/f7biQBOz6MkNC651r7Oj0b/PRX5ZWeNr3e9/8YsBO+XH00O
y2xwfuWWDC1ijyuFfC7HDhQfMMq8g/M7juvN8jMkeMtqjTDw+YTbbya10Rbv
Z6e6J+X3cb3b23FQzZGEayqSqn74vOPZH53725WEyhpHKSKVRm+ljqxMcyOB
M0vvbSe26mGR6APuJDBG/Cdy0mjUXxPs/9CDhN+XKg5YPMDnp9xrFhZeJFTo
G3bUPsT54H6CB/mSsHPZ0oKRfBq9XCJ/NuAOCZt8a7i1y/D7Mw/YtyaUBL9C
/mvr8fnwTurYplpsHSsLbRl8fjy+pZjivYfrx809Dwl83vx1Zk9mKI3H27Pq
4EANjTa8cBkLiyBhX+Cq25+aaZRsX++cmkSCrIbZ3U+faBRUtOH4jmQSFgwd
rBc+43rPCNcfwq4e5UqQHMD7OcJKdE0qCenB9kfNh2j0oXzmSVY6CY+FpIeG
R2kkKik3+ziLhCuuDNg1jetnzRXvsickrLlX0qjEZCH1AMtvF4twvTIeuHef
m4U+7jC0lCkmwVJHQX6Oh4U2V4ht9XxKwpMq48JmfF7/+uLZT43nJAh0BL9g
ibCQScE/xwxEwkPLtbHOsiwkER9icbeehH9axx/G/cdCVRZX67c14Hpxu+iH
7hYWcl5+YssYdp7apObQVhaqj1QVN35DwvoNp59v0mGh66zWNo4WEsS+Swx/
38FC/YHiu13e4fryrV+10pSFsi+nbrb8RMKR1H67f04sNKi/afk/7IHmxR2v
nVlIVrTmT/JnEjYbx/KHXmEhVuHXVyP9JLyyvnxGwY2F3Gc2Gl4ZIkH33H3r
M94sZOhadjD4KwkrH/VOGIWyUJ9Hr33JDK6vB1NK6By8nt1Oey1+kuCVECEd
9YiFTJdzqP7Fru2+nZSEv5eqnytOGvwmwfhziGDJExbK+ePg1jyH873+8AvG
SxbyuD5/c/gvCRORF8bm37CQqK94/FIeCorFGqcSJ1lovMtw3xNs9YifVv3T
eDy1q4uHeClor1MSXfMLz/dTywk2HwUthyMPPZ3H70c3WFZEgAJNHour01xs
lPV3IVFQlIKti40L3WJspO/Vn8IrQ4G8YdK0lw4bSb8VOpSF3aU+vYZfj41+
rtfl2i1LwfhgwXC8ARtldseevi1HwejxOv46YzYS0D6syK1AQabelu06h9jo
w1xNOrmGgkKbX8YP7NjI2S03k7GRAtMY3RDvcDYSMrDqy8V25BF6lhLFRo8E
hMUs1CloiM8wqI9lo/H0qzeLNChoct13fWUKG9m91T5pr0WBgdOd4b+P2OjU
xnrRt9oUmPW9rbSsY6P9Y8PeGUYU5Kn0X25mhKHvxVFFZjsp+EnpX7clwtAd
v93jHMYUPk8GneLgDkP1ko+OWe6ioM54skJfEH8PG7toCu+lQEfg46UZmTCk
k8711fMgBaFTdw+LbwtDKlYrDptYUvBV48TYtWthyHzbtb8T2O0TV/IP3AhD
/pL4q+sEBRwnhQ8p3QxDPe0h829P4v8vzuoNB+PnG/9OtLCmYGdhlO69mDA0
vrHpq50tBWKMmujk52Eoh/DyuXmJgrYxG/0vi2Go63OH0ipnChQC/G/pcYYj
Am1sr8amxQu2JVLhyNJjZBXvFQpS9glK2wmGI/7vpg33XSjouFejriYfjhw7
lcQSPCkw8X/vlWAYjtZndWaX+FOwY0XcASlWOJL+U9R97hYFtWahgu6R4Uhg
bxifeAB+3gDq/xAXjn58O+DgEkhB0Ib84YKMcFS8sX6dejAFH2zu78t9GY70
n714mH2Pgh79ZSLtX8PRsdrkjIQYChTfdZfJ745AuyV9OvfFUnBS/pSs6YEI
tM3+JHMR+5ShR/HtIxFIll/qnEU8BbvFRcsETkegwf1hqyWSKPirIGLt7BWB
LnUGpNPpFFwTW2WTkh+BAocdUv3zKdhPz5gQcpFIAg0nqz3G8dRU1BVaHYmy
Iq2TerBPTTFZiiqRqNH4aLxmIQVTflUsW+1IJJKtHzlYhPeDffP5g+aRKM5J
MmRHKQUHnJTFmtiRKH+2+jJRTcGJF72Z1cJRSK9Vzzkf+5gu++GcZBRqe/jC
ybKGgk0dod1aClHo5/ECxye1eP8VFi5rUY9CW1/G255poEALBZXeMItCVb5X
j1e1UFDKHSg8EB2F3vMq6Pt3U2DD7+S2TSUazc4YFir24Hztf7hMQysaifWd
V3yNrdHqlqqpG41MCx9T1EcKRD7ZvjM/GI0aLfQaAj/h+ecE3V/jGo1e5Zwy
CxnC8StmdX8vj0ZpJom2rAkK9nHmxT+2iUEVmys71X9QYN1Uw6N8IQZ9Uhgx
bscuLBr5VegWg2R+q65fOoXjPeVXOH4nBkUkvpoIn6GAaos4QZTEoNs/et2j
5ygwdM+40i8Uiy6ypEOSuJhwrdb/uVhTLDL9UelnQjCh4KBKzI3uWKRl4uC5
gB0/wHdh4UssWuR5ft6cYsJe305anYhDd3yPGgvxMCFPeKVbuHYcyrrEIn0E
mBDSb7rN8lEcGjHh9rGSZAL7+JKj29PjUX1OntuS5Uzov62q6l0Ujx7xHnUq
xUaxRfLvX8cj1+r0k+LSTLiR8GDPx+F4ROkYbG+WZYL4NZlDyusSkKKK97yO
IhM20MdGBYoT0Cm+mauyG5igUdHg1zmYiFo9d/SVYRuWBgcF/U5E+l/Ddp1W
Y0Lk0LSIMpmEVtZtks3cyASOWvbKdRJJaDjArWajJhN+TDRIDuokoQucfyQN
tzDhi++iovO9JOQ5x4EcDJjgcKjYXFInGY3ZmSoJ7GDCEuNY26xdyciyK4Wd
j11PyGw/cDgZ6Tw1sPtpiNd7mEOm2zEZcbjcEr5hjOffkVA+HZeMgn5wn2Ht
Y8LEoiDXGY4UFD4iyPPsKBMmj5gJ8/amoAcfF/TMzZlgr7vc9PRYCip+N+r5
G1viNn9+11wK6qqsHN90nAnVHhWUpHgqWp7k1lJgyQTbwz7ac6apKMm8LzLH
mglq8au++7ekovz9jS17bJjgqTZ6v/ZTKio3esYzhr24bfa3ymQq6tdkX1M6
w4QrOdr5ViJpaJXwTqsHtky428x31f5IGsqqe7Q6yYEJVkZzXj5Daai0LNZK
1xHHm/OB+9LZNNT4NCjqI/bkrLZPK286+pZuwytzkQmVM5MNtWrpSM1P7Hv0
JSZcaLx3q9Q7HRVt9S5iuzDBzua8usbKB6gse9+OwBtMeLt1bY4gOwNFiL3i
0vRhQke7rZzMowzk6Kta+Qk79z9zTZe6DCRxVFB/qx8TUq8YC/FyZCJnzlbd
b/74+u4LaaNXMpHCMTPtQ8F4vD3zHI/PP0SzlVXz/7DjDTcFMIMfoibVTc9z
QpiwOctiJ8p6iDwJsS1UKBMkD9qYH/v2ELXndWo9u8eETUm1X8+7ZSF/ykJd
LpwJ5lwrA58kZaPjzg2TDdhXYhWzuWqzkVqP9mP3CCbUcHo0vp3IRj2PpdXa
IpmQ4WSxgqmXgzad+KhyK4YJfZk3fX6N5iBr7Z3/auKYYLI/KM/vZi76H3NS
9bI=
       "]]},
     Annotation[#, "Charting`Private`Tag$5413#1"]& ]}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{2., -0.05764397042017198},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}, {Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{2, 3}, {-0.05764397042017198, -0.016950843488389572`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.704017004849958*^9, 3.70410899555755*^9, 
  3.7041099279477167`*^9, 3.718617839379356*^9, 
  3.718628771551358*^9},ExpressionUUID->"7b9cf950-0133-4504-94cb-\
ad39454c2985"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{0, Automatic}, {Automatic, 28}},
FrontEndVersion->"11.1 for Linux x86 (64-bit) (April 18, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 202, 4, 32, "Input", "ExpressionUUID" -> \
"99fad2fb-71ea-466d-9de2-5682f42b3bbf"],
Cell[CellGroupData[{
Cell[785, 28, 212, 4, 66, "Chapter", "ExpressionUUID" -> \
"47f4cdb3-e3a9-4a97-8ea1-a6696ac927de"],
Cell[CellGroupData[{
Cell[1022, 36, 1220, 36, 123, "Input", "ExpressionUUID" -> \
"3f99533c-50ce-4afb-a072-6ca4bdd629c9"],
Cell[2245, 74, 689, 20, 48, "Output", "ExpressionUUID" -> \
"991ed0e4-cb3b-400c-905d-031bd67b4722"],
Cell[2937, 96, 765, 22, 66, "Output", "ExpressionUUID" -> \
"cfaa252c-6e2c-4dbd-b26e-4a851c7fb4cf"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3739, 123, 354, 8, 32, "Input", "ExpressionUUID" -> \
"11fd3188-3fd3-4387-8dbe-0d77fecc32ea"],
Cell[4096, 133, 191, 3, 32, "Output", "ExpressionUUID" -> \
"d4170342-1420-4879-839a-deaaf27d610f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4324, 141, 704, 22, 34, "Input", "ExpressionUUID" -> \
"6713f908-7971-4fe5-b348-1b7fbc5293e6"],
Cell[5031, 165, 3910, 111, 332, "Output", "ExpressionUUID" -> \
"e3d16413-8712-4653-8670-c5897d79d8af"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8978, 281, 1728, 48, 130, "Input", "ExpressionUUID" -> \
"5b120867-c89d-40e4-9fcc-8fe3086bad8b"],
Cell[10709, 331, 591, 18, 41, "Output", "ExpressionUUID" -> \
"ec6b99d9-50a7-43b8-940e-5fb4be631688"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11337, 354, 1619, 46, 130, "Input", "ExpressionUUID" -> \
"3efae383-aae3-4bcf-ae61-18df831a81a8"],
Cell[12959, 402, 542, 16, 41, "Output", "ExpressionUUID" -> \
"2265118d-5484-45f4-9834-0643761e2391"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13538, 423, 646, 20, 34, "Input", "ExpressionUUID" -> \
"0881ca8d-11ba-4da3-9144-88f3d49034c3"],
Cell[14187, 445, 188, 6, 55, "Output", "ExpressionUUID" -> \
"fa9686e1-5122-4bde-a54c-fa0ce7c0226a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14412, 456, 598, 18, 34, "Input", "ExpressionUUID" -> \
"b1adaaa5-f965-42a8-80f8-f9de652dc4cf"],
Cell[15013, 476, 259, 7, 68, "Output", "ExpressionUUID" -> \
"ace4ec19-c0e4-4770-b354-3d1ad368113b"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15309, 488, 1535, 45, 102, "Input", "ExpressionUUID" -> \
"0dd3b153-d34c-4f84-89a8-e524e7104289"],
Cell[16847, 535, 886, 29, 82, "Output", "ExpressionUUID" -> \
"9f2c7114-cb94-4912-8889-713f236f65b9"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17770, 569, 1338, 41, 126, "Input", "ExpressionUUID" -> \
"f1cf41a7-5c8e-4599-90dd-4fd3029174f5"],
Cell[19111, 612, 1267, 43, 114, "Output", "ExpressionUUID" -> \
"a1211f71-ea8c-49ec-a12d-76c003c836a4"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20415, 660, 1369, 40, 94, "Input", "ExpressionUUID" -> \
"4dd6905b-d86a-4fad-b7e9-5162561dff9a"],
Cell[21787, 702, 356, 6, 32, "Output", "ExpressionUUID" -> \
"22775022-73f5-4f3b-802c-a66ac6a73ddc"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22180, 713, 186, 4, 32, "Input", "ExpressionUUID" -> \
"ce7fc89e-eca0-4e9b-891b-cfaee2c1d978"],
Cell[22369, 719, 246, 6, 52, "Output", "ExpressionUUID" -> \
"3db690c0-992c-4b10-9a45-c38c1c6d437f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22652, 730, 353, 9, 34, "Input", "ExpressionUUID" -> \
"d57ff73a-d8ed-4c19-8029-99717c5cb4ca"],
Cell[23008, 741, 9071, 167, 223, "Output", "ExpressionUUID" -> \
"7b9cf950-0133-4504-94cb-ad39454c2985"]
}, Open  ]]
}, Open  ]]
}
]
*)

